		<?php $this->load->helper('url');		?>
		<!-- HOME -->
		<section id="home" class="module-hero module-parallax module-fade module-full-height bg-dark-50" data-background="assets/images/section-1.jpg">

			<div class="hs-caption container">
				<div class="caption-content">
					<div class="hs-title-size-3 font-alt m-b-20">
						Project name
					</div>
					<div class="hs-title-size-1 font-inc">
						Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean<br> commodo ligula eget dolor. Aenean massa.
					</div>
				</div>
			</div>

		</section >
		<!-- /HOME -->

		<!-- WRAPPER -->
		<div class="wrapper">

			<!-- PORTFOLIO DESCRIPTION -->
			<section class="module-small">

				<div class="container">

					<div class="row">

						<div class="col-sm-6">

							<div class="work-details">
								<h3 class="work-details-title font-alt">Description</h3>
								<ul>
									<li class="font-inc"><strong>Client: </strong>SomeCompany</li>
									<li class="font-inc"><strong>Date: </strong>23 November, 2015</li>
									<li class="font-inc"><strong>Online: </strong><a href="#" target="_blank">www.example.com</a></li>
								</ul>
							</div>

						</div>

						<div class="col-sm-6">
							<p>The languages only differ in their grammar, their pronunciation and their most common words. Everyone realizes why a new common language would be desirable: one could refuse to pay expensive translators.</p>
							<p>The languages only differ in their grammar, their pronunciation and their most common words. Everyone realizes why a new common language would be desirable: one could refuse to pay expensive translators.</p>
						</div>

					</div>
					<!-- PORTFOLIO FILTER -->
				<div class="row">
					<div class="col-sm-12">

						<ul id="filters" class="filter font-inc hidden-xs">											
							<li>
								<a href="#" data-filter="*" class="current wow fadeInUp">
									All</a>								
								</li>
								<?php $incr_s = 0.2;
								foreach($categories[0] as $value){?>
								<li><a href="<?php echo base_url('/portfolio/category/'.$value->categoryid);?>" data-filter=".<?php echo $value->slug;?>" class="wow fadeInUp" data-wow-delay="<?php echo $incr_s?>s"><?php echo $value->name;?></a>
									<?php //if(!empty($categories[$value->categoryid])){?>
										<!-- <ul>
											 <?php foreach($categories[$value->categoryid] as $subvalue){?>
											<li><a href="<?php echo base_url('/portfolio?categoryid='.$subvalue->categoryid);?>"><?php echo $subvalue->name;?></a></li>
											<?php }?>
										</ul> -->
										<?php// }?>
									</li>
									<?php $incr_s=$incr_s+0.2;}?>
								</ul>
							</div>
						</div>
						<!-- /PORTFOLIO FILTER -->


				</div>

			</section>

			<!-- /PORTFOLIO DESCRIPTION -->

			<!-- PORTFOLIO CONTENT -->
			<section class="module p-t-0 p-b-0">

				<!-- PORTFOLIO LIST -->
				<ul id="works-grid" class="works-grid works-grid-masonry works-grid-2 works-hover-w">
					<?php 	
				// echo $this->config->base_url();exit;							
					$extensions = array('jpg', 'jpeg', 'png', 'gif', 'bmp');


					$dir_path =  '/assets/images/portfolio/'.$categories_unsorted[$categories_unsorted[$categoryid]->parentid]->name.'/'.$categories_unsorted[$categories_unsorted[$categoryid]->categoryid]->name.'/';
					$directory = new DirectoryIterator(getcwd().$dir_path);
				// $files = scandir(getcwd().$dir_path);				
				// foreach($files as $value){
					$img_path = base_url('/assets/images/portfolio').'/'.$categories_unsorted[$categories_unsorted[$categoryid]->parentid]->name.'/'.$categories_unsorted[$categories_unsorted[$categoryid]->categoryid]->name.'/';					
					$index = 1;
					foreach ($directory as $value) {
						if(($index%2)=="0"){
							$add_class = 'even-item';
						} else {
							$add_class = 'odd-item';
						}
						$index++;
					// if($value!="." && $value!=".." && is_file($value)){
						if ($value->isFile() && $value->getFilename()!='featured.jpg') {
							$extension = strtolower(pathinfo($value->getFilename(), PATHINFO_EXTENSION));
							if (in_array($extension, $extensions)) {
								// $img_file = $categories_unsorted[$categories_unsorted[$categoryid]->parentid]->name.'/'.$categories_unsorted[$categories_unsorted[$categoryid]->categoryid]->name.'/'.$value;
								$img_url = $img_path.$value;								
								?>
								<!-- PORTFOLIO ITEM -->
								<li class="work-item <?php echo $add_class;?>">
									<a href="<?php echo $img_url;?>" class="popup" title="Work 1">
										<div class="work-image">
											<img src="<?php echo $img_url;?>" alt="">
										</div>
										<div class="work-caption">
											<h3 class="work-title font-alt">
												<span class="icon-magnifying-glass"></span>
											</h3>
										</div>
									</a>
								</li>
								<!-- /PORTFOLIO ITEM -->
								<?php }}}?>
							</ul>

						</section>

						<!-- PAGINATION -->
						<section class="module-small">

							<div class="container">

								<div class="pagination font-inc text-uppercase">

									<a href="#"><i class="fa fa-angle-left"></i> Prev</a>
									<a href="#">All works</a>
									<a href="#">Next <i class="fa fa-angle-right"></i></a>

								</div>

							</div>

						</section>