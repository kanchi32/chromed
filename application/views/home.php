		<?php $this->load->helper('url');		?>
		<section class="module module-header bg-dark bg-dark-50" data-background="<?php echo base_url('assets/images/section-4.jpg')?>" style="background-image: url("/chromed/assets/images/section-7.jpg");">

			<div class="container">

				<!-- MODULE TITLE -->
				<div class="row">

					<div class="col-sm-8 col-sm-offset-2">

						<h1 class="module-title font-alt align-center">Content box</h1>

					</div>

				</div>
				<!-- /MODULE TITLE -->

			</div>

		</section>
		<!-- PORTFOLIO -->
		<section class="module-small p-t-20 p-b-60">

			<div class="container">

				<!-- PORTFOLIO FILTER -->
				<div class="row">
					<div class="col-sm-12">

						<ul id="filters" class="filter font-inc hidden-xs">											
							<li>
								<a href="#" data-filter="*" class="current wow fadeInUp">
									All</a>								
								</li>
								<?php $incr_s = 0.2;
								foreach($categories[0] as $value){?>
								<li><a href="<?php echo base_url('/portfolio/category/'.$value->categoryid);?>" data-filter=".<?php echo $value->slug;?>" class="wow fadeInUp" data-wow-delay="<?php echo $incr_s?>s"><?php echo $value->name;?></a>
									<?php //if(!empty($categories[$value->categoryid])){?>
										<!-- <ul>
											 <?php foreach($categories[$value->categoryid] as $subvalue){?>
											<li><a href="<?php echo base_url('/portfolio?categoryid='.$subvalue->categoryid);?>"><?php echo $subvalue->name;?></a></li>
											<?php }?>
										</ul> -->
										<?php// }?>
									</li>
									<?php $incr_s=$incr_s+0.2;}?>
								</ul>
							</div>
						</div>
						<!-- /PORTFOLIO FILTER -->

						<!-- PORTFOLIO LIST -->
						<!-- <ul id="works-grid" class="works-grid works-grid-gut works-grid-2 works-hover-w"> -->
						<ul id="works-grid" class="works-grid works-grid-3 works-grid-gut works-hover-w">
							<!-- <ul id="works-grid" class="works-grid works-grid-gut works-grid-3 works-hover-w">						 -->
							<!-- /PORTFOLIO ITEM -->
<?php //if($categoryid>"0"){
//}
	// print_r($categories);echo '<br>';
	foreach($categories[$categoryid] as $value){
		// print_r($value);
		// echo '<br>';print_r($categories[$value->parentid]);exit;
		$img_file = $categories_unsorted[$value->parentid]->name.'/'.$value->name.'/featured.jpg';
		// $img_file = $categories[$categories_unsorted[$value->parentid]->parentid]->name.'/'.$value->name.'/featured.jpg';
		?>						
		<li class="work-item illustration webdesign">
			<a href="<?php echo base_url('/portfolio/category/'.$value->categoryid);?>">
				<div class="work-image">
					<img src="<?php echo base_url('/assets/images/portfolio/'.$img_file);?>" alt="">
				</div>
				<div class="work-caption">
					<h3 class="work-title font-alt"><?php echo $value->name?></h3>
					<div class="work-descr font-inc">
						Photography
					</div>
				</div>
			</a>
		</li>
		<?php }?>
	</ul>
</div>
</section>
<section class="module p-t-0 p-b-0" data-background="<?php echo base_url('assets/images/section-4.jpg')?>">
	<div class="container-fluid">
		<div class="row relative">
			<div class="col-sm-12 col-md-6 col-md-offset-6 col-bg">
				<h2 class="module-title font-alt">About Us</h2>
				<p>Lorem ipsum dolor Lorem ipsum dolor Lorem ipsum dolor Lorem ipsum dolor Lorem ipsum dolorLorem ipsum dolor Lorem ipsum dolor</p>
				<div class="module-subtitle font-inc">
					Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Lorem ipsum dolor Lorem ipsum dolor Lorem ipsum dolor Lorem ipsum dolor
				</div>
				<ul class="social-list">
					<li><a href="#"><span class="icon-facebook"></span></a></li>
					<li><a href="#"><span class="icon-twitter"></span></a></li>
					<li><a href="#"><span class="icon-googleplus"></span></a></li>
				</ul>
			</div>
		</div>
	</div>
</section>

<!-- /PORTFOLIO -->
<section class="module-small p-t-40 p-b-20">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<h2 class="module-title font-alt">What Our Clients Say</h2>				
			</div>
			<div class="col-sm-6">

				<div class="post-quote">
					<blockquote class="font-serif">
						<p>" The languages only differ in their grammar, their pronunciation and their most common words. Everyone realizes why a new common language would be desirable: one could refuse to pay expensive translators. "</p>
						<p class="font-inc font-uppercase">- Thomas Anderson</p>
					</blockquote>
				</div>
			</div>

			<div class="col-sm-6">
				<div class="post-quote">
					<blockquote class="font-serif">
						<p>" The languages only differ in their grammar, their pronunciation and their most common words. Everyone realizes why a new common language would be desirable: one could refuse to pay expensive translators. "</p>
						<p class="font-inc font-uppercase">- Thomas Anderson</p>
					</blockquote>
				</div>
			</div>
		</div>
	</div>
</section>				

<section class="module p-t-20 p-b-20">

	<div class="container">

		<div class="row">

			<div class="col-sm-12">			
				<h2 class="font-alt module-title font-alt">Our Esteemed Clients</h2>
				<div class="row multi-columns-row">					
					<div class="col-sm-6 col-md-4 col-lg-4">
						<div class="content-box">
							<div class="content-box-icon">
								<span class=""><img src="<?php echo base_url('assets/images/clients/housing.png')?>"></span>
							</div>
							<div class="content-box-title font-inc font-uppercase">
								Housing.com
							</div>							
						</div>
					</div>
					<!-- /CONTENT BOX -->

					<!-- CONTENT BOX -->
					<div class="col-sm-6 col-md-4 col-lg-4">
						<div class="content-box">
							<div class="content-box-icon">
								<span class="icon-video"></span>
							</div>
							<div class="content-box-title font-inc font-uppercase">
								Video
							</div>
							<div class="content-box-text">
								Everyone realizes why a common language would be desirable: one could refuse to pay expensive translators.
							</div>
						</div>
					</div>
					<!-- /CONTENT BOX -->

					<!-- CONTENT BOX -->
					<div class="col-sm-6 col-md-4 col-lg-4">
						<div class="content-box">
							<div class="content-box-icon">
								<span class="icon-tools"></span>
							</div>
							<div class="content-box-title font-inc font-uppercase">
								Design
							</div>
							<div class="content-box-text">
								Everyone realizes why a common language would be desirable: one could refuse to pay expensive translators.
							</div>
						</div>
					</div>
					<!-- /CONTENT BOX -->

				</div>
				<!-- /CONTENT BOXES -->

			</div>

		</div>

	</div>

</section>
<!-- / COMPONENTS  -->
<hr class="divider-w">

<!-- BLOG GRID -->
<section class="module p-t-20 p-b-20">

	<div class="container">
		<h2 class="font-alt module-title font-alt">Fresh from our Blog</h2>
		<div class="row multi-columns-row post-columns">

			<!-- POST ITEM -->
			<div class="col-sm-6 col-md-4 col-lg-4">
				<div class="post">
					<div class="post-thumbnail">
						<a href="blog-single.html"><img src="<?php echo base_url('assets/images/post-1.jpg')?>" alt=""></a>
					</div>
					<div class="post-header">
						<h2 class="post-title font-alt"><a href="blog-single.html">Our trip to the Alps</a></h2>
						<div class="post-meta font-inc">
							By <a href="#">Thomas Anderson</a> | 31 DEC 2014
						</div>
					</div>
					<div class="post-entry">
						<p>A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart.</p>
					</div>
					<div class="post-more font-inc">
						<a href="blog-single.html" class="more-link">Read more</a>
					</div>
				</div>
			</div>
			<!-- /POST ITEM -->

			<!-- POST ITEM -->
			<div class="col-sm-6 col-md-4 col-lg-4">
				<div class="post">
					<div class="post-thumbnail">
						<a href="blog-single.html"><img src="<?php echo base_url('assets/images/post-1.jpg')?>" alt=""></a>
					</div>
					<div class="post-header">
						<h2 class="post-title font-alt"><a href="blog-single.html">Shore after the tide</a></h2>
						<div class="post-meta font-inc">
							By <a href="#">Mark Stone</a> | 25 DEC 2014
						</div>
					</div>
					<div class="post-entry">
						<p>A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart.</p>
					</div>
					<div class="post-more font-inc">
						<a href="blog-single.html" class="more-link">Read more</a>
					</div>
				</div>
			</div>
			<!-- /POST ITEM -->

			<!-- POST ITEM -->
			<div class="col-sm-6 col-md-4 col-lg-4">
				<div class="post">
					<div class="post-thumbnail">
						<a href="blog-single.html"><img src="<?php echo base_url('assets/images/post-1.jpg')?>" alt=""></a>
					</div>
					<div class="post-header">
						<h2 class="post-title font-alt"><a href="blog-single.html">We in New Zealand</a></h2>
						<div class="post-meta font-inc">
							By <a href="#">Thomas Anderson</a> | 22 DEC 2014
						</div>
					</div>
					<div class="post-entry">
						<p>A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart.</p>
					</div>
					<div class="post-more font-inc">
						<a href="blog-single.html" class="more-link">Read more</a>
					</div>
				</div>
			</div>
			<!-- /POST ITEM -->

		</div>

	</section>
	<hr class="divider-w">
	<section class="module-small p-t-20 p-b-20">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<h2 class="module-title font-alt">Contact</h2>
				
				<div class="col-sm-6">					
						<div id="map-section" class="" style="max-height:300px;">
							<div id="map"></div>
						</div>					
				</div>
				<div class="col-sm-6">
					<form id="contact-form" role="form" novalidate="">
						<div class="form-group">
							<label class="sr-only" for="cname">Name</label>
							<input type="text" id="cname" class="form-control" name="cname" placeholder="Name" required="" data-validation-required-message="Please enter your name." aria-invalid="false">
							<p class="help-block text-danger"></p>
						</div>
						<div class="form-group">
							<label class="sr-only" for="cemail">Your Email</label>
							<input type="email" id="cemail" name="cemail" class="form-control" placeholder="Your E-mail" required="" data-validation-required-message="Please enter your email address.">
							<p class="help-block text-danger"></p>
						</div>
						<div class="form-group">
							<textarea class="form-control" id="cmessage" name="cmessage" rows="7" placeholder="Message" required="" data-validation-required-message="Please enter your message."></textarea>
							<p class="help-block text-danger"></p>
						</div>
						<button type="submit" class="btn btn-round btn-g">Submit</button>
					</form>
					<div id="contact-response" class="ajax-response font-alt"></div>					
				</div>
				</div>
			</div>
		</div>
	</section>				