<section class="module module-header bg-dark bg-dark-50" data-background="assets/images/section-7.jpg">

	<div class="container">

		<!-- MODULE TITLE -->
		<div class="row">

			<div class="col-sm-8 col-sm-offset-2">

				<h1 class="module-title font-alt align-center">Post title</h1>

				<div class="module-subtitle font-inc align-center">
					<div class="post-meta font-inc">
						By <a href="#">Thomas Anderson</a> | 31 DEC 2014 | 3 Comments | <a href="#">Photography</a>, <a href="#">Web Design</a>
					</div>
				</div>

			</div>

		</div>
		
	</div>

</section >
<!-- /HOME -->

<!-- BLOG SINGLE -->
<section class="module-small p-b-20" id='work'>

	<div class="container">

		<div class="row" >

			<div class="col-sm-8 col-sm-offset-2" >
				<h2 class="module-title align-center font-alt">How we work</h2>
				<!-- POST -->
				<div class="">
					<div class="post-entry">

						<p>The European languages are members of the same family. Their separate existence is a myth. For science, music, sport, etc, Europe uses the same vocabulary. The languages only differ in their grammar, their pronunciation and their most common words.</p>
						<p>Everyone realizes why a new common language would be desirable: one could refuse to pay expensive translators. To achieve this, it would be necessary to have uniform grammar, pronunciation and more common words.</p>
						<blockquote>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
						</blockquote>
						<ul>
							<li>The European languages are members of the same family.</li>
							<li>Their separate existence is a myth.</li>
							<li>For science, music, sport, etc, Europe uses the same vocabulary.</li>
						</ul>
					</div>

				</div>
				<!-- /POST -->
			</div>			
		</div>	
	</div>
</section>	
<hr class="divider-w">
<section class="module-small p-b-20" id='mission'>
	<div class="container">
		<div class="row" >
			<div class="col-sm-8 col-sm-offset-2" id='mission'>
				<h2 class="module-title align-center font-alt">Our Mission</h2>
				<!-- POST -->
				<div class="">
					<div class="post-entry">

						<p>The European languages are members of the same family. Their separate existence is a myth. For science, music, sport, etc, Europe uses the same vocabulary. The languages only differ in their grammar, their pronunciation and their most common words.</p>
						<p>Everyone realizes why a new common language would be desirable: one could refuse to pay expensive translators. To achieve this, it would be necessary to have uniform grammar, pronunciation and more common words.</p>
						<blockquote>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
						</blockquote>
						<ul>
							<li>The European languages are members of the same family.</li>
							<li>Their separate existence is a myth.</li>
							<li>For science, music, sport, etc, Europe uses the same vocabulary.</li>
						</ul>
					</div>

				</div>
				<!-- /POST -->
			</div>
		</div>	
	</div>
</section>	
<hr class="divider-w">
<section class="module-small p-b-20" id='team'>

	<div class="container">

		<!-- MODULE TITLE -->
		<div class="row" >

			<div class="col-sm-6 col-sm-offset-3">

				<h2 class="module-title align-center font-alt">Our Team</h2>

				<div class="module-subtitle align-center font-inc">
					A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart.
				</div>

			</div>
		</div>
		<div class="row">		
			<!-- TEAM MEMBER -->
			<div class="col-sm-6 col-md-3 m-b-md-20">
				<div class="team-item">
					<div class="team-image">
						<img src="assets/images/team-4.jpg" alt="">
						<div class="team-detail">
							<h5 class="font-inc">Hi all</h5>
							<p>The languages only differ in their grammar, their pronunciation and their most common words.</p>
							<p>abhigyan@chromed.in</p>
							<ul class="social-list">
								<li><a href="#"><span class="icon-facebook"></span></a></li>
								<li><a href="#"><span class="icon-twitter"></span></a></li>
								<li><a href="#"><span class="icon-googleplus"></span></a></li>
							</ul>
						</div>
					</div>
					<div class="team-descr font-inc">
						<div class="team-name">Abhigyan Neogi</div>
						<div class="team-role">Principal Architect</div>
					</div>
				</div>
			</div>
			<!-- /TEAM MEMBER -->
			<div class="col-sm-6 col-md-3 m-b-md-20">			
				<div class="team-item">
					<div class="team-image">
						<img src="assets/images/team-2.jpg" alt="">
						<div class="team-detail">
							<h5 class="font-inc">Hi all</h5>
							<p>The languages only differ in their grammar, their pronunciation and their most common words.</p>
							<p>priyanka@chromed.in</p>
							<ul class="social-list">
								<li><a href="#"><span class="icon-facebook"></span></a></li>
								<li><a href="#"><span class="icon-twitter"></span></a></li>
								<li><a href="#"><span class="icon-googleplus"></span></a></li>
							</ul>
						</div>
					</div>
					<div class="team-descr font-inc">
						<div class="team-name">Priyanka Mahajan</div>
						<div class="team-role">Associate Architect</div>
					</div>
				</div>
			</div>
			<!-- /TEAM MEMBER -->

			<!-- TEAM MEMBER -->
			<div class="col-sm-6 col-md-3 m-b-md-20">
				<div class="team-item">
					<div class="team-image">
						<img src="assets/images/team-3.jpg" alt="">
						<div class="team-detail">
							<h5 class="font-inc">Hi all</h5>
							<p>The languages only differ in their grammar, their pronunciation and their most common words.</p>
							<p>nidhi@chromed.in</p>
							<ul class="social-list">
								<li><a href="#"><span class="icon-facebook"></span></a></li>
								<li><a href="#"><span class="icon-twitter"></span></a></li>
								<li><a href="#"><span class="icon-googleplus"></span></a></li>
							</ul>
						</div>
					</div>
					<div class="team-descr font-inc">
						<div class="team-name">Nidhi Goyal</div>
						<div class="team-role">Associate Architect</div>
					</div>
				</div>
			</div>
			<!-- /TEAM MEMBER -->
			<!-- TEAM MEMBER -->
			<div class="col-sm-6 col-md-3 m-b-md-20">
				<div class="team-item">
					<div class="team-image">
						<img src="assets/images/team-1.jpg" alt="">
						<div class="team-detail">
							<h5 class="font-inc">Hi all</h5>
							<p>The languages only differ in their grammar, their pronunciation and their most common words.</p>
							<p>memanishchawala@gmail.com</p>
							<ul class="social-list">
								<li><a href="#"><span class="icon-facebook"></span></a></li>
								<li><a href="#"><span class="icon-twitter"></span></a></li>
								<li><a href="#"><span class="icon-googleplus"></span></a></li>
							</ul>
						</div>
					</div>
					<div class="team-descr font-inc">
						<div class="team-name">Manish Chawla</div>
						<div class="team-role">Website Designer</div>
					</div>
				</div>
			</div>
			<!-- /TEAM MEMBER -->
			<div class="col-sm-6 col-md-3 m-b-md-20">
				<div class="team-item">
					<div class="team-image">
						<img src="assets/images/team-1.jpg" alt="">
						<div class="team-detail">
							<h5 class="font-inc">Hi all</h5>
							<p>The languages only differ in their grammar, their pronunciation and their most common words.</p>
							<p>aditya@marketingsolutions.inc</p>
							<ul class="social-list">
								<li><a href="#"><span class="icon-facebook"></span></a></li>
								<li><a href="#"><span class="icon-twitter"></span></a></li>
								<li><a href="#"><span class="icon-googleplus"></span></a></li>
							</ul>
						</div>
					</div>
					<div class="team-descr font-inc">
						<div class="team-name">Aditya Raut</div>
						<div class="team-role">Graphic Designer</div>
					</div>
				</div>
			</div>	
			<!-- TEAM MEMBER -->			
		</div>

	</div>

</section>