<?php $this->load->helper('url');		?>
<!-- PORTFOLIO -->
<section class="module-small p-t-20 p-b-60">

	<div class="container">
		
		<div class="row">
			<h2>Manage Category</h2>
			<div class="col-sm-6">
				<form id="login-form" role="form" novalidate="" method="POST" action="/chromed/login">

					<div class="col-sm-8 col-sm-offset-8">
						<h4 class="font-alt m-t-0 m-b-0">Add Category</h4>
						<hr class="divider-w m-t-10 m-b-20">
						<div class="form-group">
							<label class="sr-only" for="category">Email</label>
							<input style='text-transform:none;' class="form-control input-lg" type="text" placeholder="Category Name" required="" data-validation-required-message="Please enter Category Name." aria-invalid="false" name='category'>
							<p class="help-block text-danger"></p>
						</div>
						<?php if (isset($category_error)) echo $category_error;?></p>
						<button type="submit" class="btn btn-round btn-g">Submit</button>
					</div>
				</form>
			</div>
			<div class="col-sm-12">
				<?php
				$loop_categories[] = $categoryid;
				?>
				<table class="table">
					<?php if($categoryid!=0){?>
					<caption>Category:
						<?php					
						if($categories_unsorted[$categoryid]->parentid!=0){?>
						<a href="<?php echo base_url('/manage/portfolio/'.$categories_unsorted[$categories_unsorted[$categoryid]->parentid]->categoryid);?>">
							<?php echo $categories_unsorted[$categories_unsorted[$categoryid]->parentid]->name.' > ';?></a>
							<?php }?>
							<a href="<?php echo base_url('/manage/portfolio/'.$categories_unsorted[$categoryid]->categoryid);?>">
								<?php echo $categories_unsorted[$categoryid]->name;?>
							</a>					
						</caption>
						<?php }?>
						<thead>
							<tr>
								<th>Category</th>
								<th>Actions</th>
							</tr>
						</thead>
						<tbody>
							<?php		
							foreach ($loop_categories as $categoryidkey) {	
								if(isset($categories[$categoryidkey])){
									foreach($categories[$categoryidkey] as $value){			
										?>
										<tr>
											<td><a href="<?php echo base_url('/manage/portfolio/'.$value->categoryid);?>" data-filter=".<?php echo $value->slug;?>" class="wow fadeInUp" ><?php echo $value->name;?></a>
											</td>
											<td></td>
										</tr>
										<?php }} else {echo 'here';}}?>
									</tbody>
								</table>
					<?php //} else {

						// echo 'here';
					// }?>


				</div>
			</div>
		</div>
	</section>