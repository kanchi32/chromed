<?php $this->load->library('session');
$this->load->helper('url');
?>
<!DOCTYPE html>
<html lang="en">
<head>

	<meta charset="UTF-8">

	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>Semantic - Minimal portfolio template</title>

	<!-- Bootstrap core CSS -->
	<link href="/chromed/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">


	<!-- Plugins -->

	<link href="/chromed/assets/css/simpletextrotator.css" rel="stylesheet">

	<link href="/chromed/assets/css/font-awesome.min.css" rel="stylesheet">

	<link href="/chromed/assets/css/et-line-font.css" rel="stylesheet">

	<link href="/chromed/assets/css/magnific-popup.css" rel="stylesheet">
	<link href="/chromed/assets/css/flexslider.css" rel="stylesheet">
	<link href="/chromed/assets/css/animate.css" rel="stylesheet">

	<!-- Template core CSS -->
	<link href="/chromed/assets/css/style.css" rel="stylesheet">


	<!-- Custom css -->

	<link href="/chromed/assets/css/custom.css" rel="stylesheet">
</head>
<body>


	<!-- PRELOADER -->

	<div class="page-loader">

		<div class="loader">Loading...</div>

	</div>

	<!-- /PRELOADER -->


	<!-- NAVIGATION -->

	<nav class="navbar navbar-custom navbar-transparent navbar-fixed-top" role="navigation">

		
		<div class="container">

			
			<div class="navbar-header">
				
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#custom-collapse">
					
					<span class="sr-only">Toggle navigation</span>
					
					<span class="icon-bar"></span>
					
					<span class="icon-bar"></span>
					
					<span class="icon-bar"></span>

				</button>
				
				<a class="navbar-brand" href="index.html">Semantic</a>

			</div>

			
			<div class="collapse navbar-collapse" id="custom-collapse">

				
				<ul class="nav navbar-nav navbar-right">

					
					<li class="dropdown">
						
						<a href="<?php echo site_url('/home');?>" >Home</a>
						
						<!-- <ul class="dropdown-menu">
							
							<li><a href="index.html">Image Parallax 1</a></li>
							
							<li><a href="index-2.html">Image Parallax 2</a></li>
							
							<li><a href="index-3.html">Slider Parallax 1</a></li>
							
							<li><a href="index-4.html">Slider Parallax 2</a></li>
							
							<li><a href="index-5.html">Text rotator 1</a></li>
							
							<li><a href="index-6.html">Text rotator 2</a></li>
							
							<li><a href="index-7.html">Video background 1</a></li>
							
							<li><a href="index-8.html">Video background 2</a></li>
						
						</ul> -->

					</li>

					
					<li class="dropdown">
					<a href="<?php echo base_url('/about');?>" class="dropdown-toggle" data-toggle="dropdown">About</a>
						<ul class="dropdown-menu" role="menu">
							<li>
								<a href="<?php echo base_url('/about#work');?>" >How we work</a>
							</li>
							<li>
								<a href="<?php echo base_url('/about#mission');?>">Mission</a>
							</li>
							<li>
								<a href="<?php echo base_url('/about#team');?>">Team</a>
							</li>
						</ul>
					</li>
					<li class="dropdown">
						
						<a href="<?php echo base_url('/portfolio');?>" class="dropdown-toggle" data-toggle="dropdown">Portfolio</a>
						
						<ul class="dropdown-menu" role="menu">

							
							<li class="dropdown">
								
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">Filtering</a>
								
								<ul class="dropdown-menu">
									
									<li><a href="portfolio-boxed-col2.html">S1</a></li>
									
									<li><a href="portfolio-boxed-col3.html">S2</a></li>
									
									<li><a href="portfolio-boxed-col4.html">S3</a></li>

								</ul>

							</li>
							
							<li>								
								<a href="#">Case Study</a>																
							</li>
						</ul>
					</li>
					

					<li class="dropdown">
						
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">Clients</a>
						
						<ul class="dropdown-menu" role="menu">
						<li><a href="blog-single-2.html">Esteemed Clients</a></li>
						<li><a href="blog-single-2.html">Testimonials</a></li>
							
							<!-- <li><a href="comp-alerts-wells.html"><i class="fa fa-bolt"></i> Alerts and Wells</a></li>
							
							<li><a href="comp-buttons.html"><i class="fa fa-link fa-sm"></i> Buttons</a></li>
							
							<li><a href="comp-collapses-tabs.html"><i class="fa fa-tasks"></i> Collapses &amp; Tabs</a></li>
							
							<li><a href="comp-content-box.html"><i class="fa fa-list-alt"></i> Contents Box</a></li>
							
							<li><a href="comp-forms.html"><i class="fa fa-check-square-o"></i> Forms</a></li>
							
							<li><a href="comp-icons.html"><i class="fa fa-star"></i> Icons</a></li>
							
							<li><a href="comp-progress-bars.html"><i class="fa fa-server"></i> Progress Bars</a></li>
							
							<li><a href="comp-typography.html"><i class="fa fa-font"></i> Typography</a></li> -->

						</ul>

					</li>
					<li><a href="<?php echo base_url('/contact')?>">Contact</a></li>

					
					<li class="dropdown">
						
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">Blog</a>
						
						<ul class="dropdown-menu" role="menu">

							<li><a href="contact.html">Articles Filter</a></li>

							<li><a href="contact.html">Tag</a></li>

							<li class="dropdown">
								
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">C1</a>
								
								<ul class="dropdown-menu">
									
									<li><a href="blog-grid-2col.html">C2</a></li>
									
									<li><a href="blog-grid-3col.html">C3</a></li>																	

								</ul>

							</li>

						</ul>

					</li>

					<li class="dropdown">						
						<a href="<?php echo base_url('/careers');?>" >Join Us</a>
					</li>
					<?php 						
					if(isset($_SESSION['userid'])) {?>
					<li class="dropdown">						
						<a href="/chromed/logout" >Logout</a>
					</li>
					<?php }?>
				</ul>

			</div>



		</div>


	</nav>

	<!-- /NAVIGATION -->


	<!-- WRAPPER -->

	<div class="wrapper">

		<?php foreach($sections as $section){
		echo $section;

	} ?>
	
	<!-- DIVIDER -->
	
	<hr class="divider-w">
	
	<!-- /DIVIDER -->
	<!-- CONTACT -->
	<section class="module-small">

		
		<div class="container">

			
			<div class="row">

				
				<!-- CONTENT BOX -->
				
				<div class="col-sm-4">
					
					<div class="content-box">
						
						<div class="content-box-icon">
							
							<span class="icon-map-pin"></span>

						</div>
						
						<div class="content-box-title font-inc">
							
							007 Steet, City, USA

						</div>

					</div>

				</div>
				
				<!-- /CONTENT BOX -->

				
				<!-- CONTENT BOX -->
				
				<div class="col-sm-4">
					
					<div class="content-box">
						
						<div class="content-box-icon">
							
							<span class="icon-phone"></span>

						</div>
						
						<div class="content-box-title font-inc">
							
							+1 234 567 89 00

						</div>

					</div>

				</div>
				
				<!-- /CONTENT BOX -->

				
				<!-- CONTENT BOX -->
				
				<div class="col-sm-4">
					
					<div class="content-box">
						
						<div class="content-box-icon">
							
							<span class="icon-envelope"></span>

						</div>
						
						<div class="content-box-title font-inc">
							
							semantic@email.com

						</div>

					</div>

				</div>
				
				<!-- /CONTENT BOX -->


			</div>


		</div>


	</section>
	
	<!-- /CONTACT -->
	<!-- FOOTER -->
	<footer class="footer">

		
		<div class="container">

			
			<div class="row">

				
				<div class="col-sm-12 text-center">
					
					<p class="copyright font-inc m-b-0">© 2015 <a href="index.html">SEMANTIC</a>, All Rights Reserved.</p>

				</div>


			</div>


		</div>


	</footer>
	
	<!-- /FOOTER -->


</div>

<!-- /WRAPPER -->


<!-- Scroll-up -->

<div class="scroll-up">
	
	<a href="#totop"><i class="fa fa-angle-double-up"></i></a>
</div>


<!-- Javascript files -->

<script src="/chromed/assets/js/jquery-2.1.3.min.js"></script>

<script src="/chromed/assets/bootstrap/js/bootstrap.min.js"></script>

<script src="/chromed/assets/js/jquery.mb.YTPlayer.min.js"></script>

<script src="/chromed/assets/js/appear.js"></script>

<script src="/chromed/assets/js/jquery.simple-text-rotator.min.js"></script>

<script src="/chromed/assets/js/jqBootstrapValidation.js"></script>

<script src="http://maps.google.com/maps/api/js?sensor=true"></script>

<script src="/chromed/assets/js/gmaps.js"></script>

<script src="/chromed/assets/js/isotope.pkgd.min.js"></script>

<script src="/chromed/assets/js/imagesloaded.pkgd.js"></script>

<script src="/chromed/assets/js/jquery.flexslider-min.js"></script>

<script src="/chromed/assets/js/jquery.magnific-popup.min.js"></script>

<script src="/chromed/assets/js/jquery.fitvids.js"></script>

<script src="/chromed/assets/js/smoothscroll.js"></script>

<script src="/chromed/assets/js/wow.min.js"></script>

<script src="/chromed/assets/js/contact.js"></script>

<script src="/chromed/assets/js/custom.js"></script>

</body>
</html>