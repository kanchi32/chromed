		<?php $this->load->helper('url');		?>
		<section class="module module-header bg-dark bg-dark-50" data-background="assets/images/section-1.jpg">

			<div class="container">

				<!-- MODULE TITLE -->
				<div class="row">

					<div class="col-sm-6 col-sm-offset-3">

						<h1 class="module-title font-alt align-center">Portfolio boxed Gutter</h1>

						<div class="module-subtitle font-inc align-center">
							A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart.
						</div>

					</div>

				</div>
				<!-- /MODULE TITLE -->

			</div>

		</section >
		<!-- /HOME -->

		<!-- PORTFOLIO -->
		<section class="module-small p-t-20 p-b-60">

			<div class="container">

				<!-- PORTFOLIO FILTER -->
				<div class="row">
					<div class="col-sm-12">

						<ul id="filters" class="filter font-inc hidden-xs">											
							<li>
								<a href="#" data-filter="*" class="current wow fadeInUp">
									All</a>								
								</li>
								<?php $incr_s = 0.2;
								foreach($categories[0] as $value){?>
								<li><a href="<?php echo base_url('/portfolio/category/'.$value->categoryid);?>" data-filter=".<?php echo $value->slug;?>" class="wow fadeInUp" data-wow-delay="<?php echo $incr_s?>s"><?php echo $value->name;?></a>
									<?php //if(!empty($categories[$value->categoryid])){?>
										<!-- <ul>
											 <?php foreach($categories[$value->categoryid] as $subvalue){?>
											<li><a href="<?php echo base_url('/portfolio?categoryid='.$subvalue->categoryid);?>"><?php echo $subvalue->name;?></a></li>
											<?php }?>
										</ul> -->
										<?php// }?>
									</li>
									<?php $incr_s=$incr_s+0.2;}?>
								</ul>
							</div>
						</div>
						<!-- /PORTFOLIO FILTER -->

						<!-- PORTFOLIO LIST -->
						<ul id="works-grid" class="works-grid works-grid-gut works-grid-2 works-hover-w">
<?php //if($categoryid>"0"){
//}
	// print_r($categories);echo '<br>';
	
	if($categoryid==0){
		$loop_categories = array_keys($categories[$categoryid]);
	} else {
		$loop_categories[] = $categoryid;
	}
	// print_r($loop_categories);exit;
	foreach ($loop_categories as $categoryidkey) {	
	foreach($categories[$categoryidkey] as $value){
		// print_r($value);
		// echo '<br>';print_r($categories[$value->parentid]);exit;
		$img_file = $categories_unsorted[$value->parentid]->name.'/'.$value->name.'/featured.jpg';
		// $img_file = $categories[$categories_unsorted[$value->parentid]->parentid]->name.'/'.$value->name.'/featured.jpg';
		?>						
		<li class="work-item marketing photography">
			<a href="<?php echo base_url('/portfolio/category/'.$value->categoryid);?>">
				<div class="work-image">
					<img src="<?php echo base_url('/assets/images/portfolio/'.$img_file);?>" alt="">
				</div>
				<div class="work-caption">
					<h3 class="work-title font-alt"><?php echo $value->name?></h3>
					<div class="work-descr font-inc">
						Photography
					</div>
				</div>
			</a>
		</li>
		<?php }}?>
	</ul>
</div>
</section>
<!-- /PORTFOLIO -->

<!-- PAGINATION -->
<section class="module-small p-t-0">

	<div class="container">

		<div class="pagination font-inc text-uppercase">

			<a href="#"><i class="fa fa-angle-left"></i> Prev</a>
			<a href="#">Next <i class="fa fa-angle-right"></i></a>

		</div>

	</div>

</section>
<!-- /PAGINATION -->
