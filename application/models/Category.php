<?php
class Category extends CI_Model {

	function __construct()
	{        
		parent::__construct();
		$this->load->database();
	}

	function get($params=array())
	{		
		$this->db->select('categoryid,name,parentid,status,slug');
		$this->db->from('category');
		if(!empty($params['where'])){
			$this->db->where($params['where']);
		}
		if(!empty($params['where_or'])){
			$this->db->or_where($params['where_or']);
		}
		$query = $this->db->get();		
		$result = $query->result();
		// echo $this->db->last_query();exit;
		if(empty($result))
			return false;
		else
			return $result;
	}
	function get_heirarchy($categoryid){
		$this->db->select('categoryid,name,parentid,status,slug');
		$this->db->from('category');
			$this->db->where($params['where']);
			$this->db->or_where($params['where_or']);
		$query = $this->db->get();
		$result = $query->result();
		// echo $this->db->last_query();exit;
		if(empty($result))
			return false;
		else
			return $result;	
	}
}