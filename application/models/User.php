<?php
class User extends CI_Model {

	function __construct()
	{
        // Call the Model constructor
		parent::__construct();
		$this->load->database();

		// $this->db = $this->load->database('group_one', TRUE);

	}

	function validate_user_credentials($params)
	{
		if(empty($params['email'])||empty($params['password'])){
			return false;
		}
		$this->db->select('userid,email,password,status');
		$this->db->from('user');
		$check['email']=$params['email'];
		$check['password']=hex2bin(md5($params['password']));
		$this->db->where($check);
		$query = $this->db->get();
		$result = $query->result();
		if(empty($result))
			return false;
		else
			return $result;
	}
}