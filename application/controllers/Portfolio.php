<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Portfolio extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('session');		
	}
	public function index()
	{
	   $this->category();
	}	
	public function category($categoryid=''){
		$categoryid = (isset($_GET['categoryid']))? $_GET['categoryid']:$categoryid;
		$data=array();
		$this->load->model('category');
		$categories = $this->category->get();
		$cat_rel=array();
		if(!empty($categories)) {
			$categories_unsorted=array();
			foreach ($categories as $value) {
				$cat_rel[$value->parentid][$value->categoryid]=$value;				
				$categories_unsorted[$value->categoryid]=$value;
			}
		}
		if(isset($categoryid) && $categoryid>0) {
			$data['categoryid']=$categoryid;
		} else {
		$categoryid=0;	
			 $data['categoryid']=0;
		}		
		$data['categories']=$cat_rel;
		$data['categories_unsorted']=$categories_unsorted;
		if(isset($cat_rel[$categoryid])){ //this category is parent of some other category.
			$data['sections'] = array($this->load->view('portfolio',$data,true));
		} else {			
			$data['sections'] = array($this->load->view('photography',$data,true));
		} 

		$this->load->view('main',$data);	
	}
}
