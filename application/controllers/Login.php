<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('session');				
		if(isset($_SESSION['userid'])) {
			if(isset($_SERVER['HTTP_REFERER']) ) {
				$redirect_url = $_SERVER['HTTP_REFERER'];
			} else {
				$redirect_url = '/chromed';
			}
			header("Location: $redirect_url");
		}
	}
public function index()
{
	if(empty($_POST)) {
		$data['sections'] = array($this->load->view('login','',true));
		$this->load->view('main',$data);
	} else {
		$this->load->model('user');
		$validate = $this->user->validate_user_credentials($_POST);
		if($validate){
			if($validate[0]->status=='active'){
					//set session								
				$this->session_login($validate[0]);
				header('Location: /chromed/manage');
			}
		}
		$data['login_error']='Invalid Email / Password';
		$data['sections'] = array($this->load->view('login',$data,true));
		$this->load->view('main',$data);
	}
}
function session_login($data){
	$user['userid']=$data->userid;
	$user['email']=$data->email;	
	$this->session->set_userdata($user);		
}
}
