<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Manage extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->model('category');
		if(!isset($_SESSION['userid'])) {
			if(isset($_SERVER['HTTP_REFERER']) ) {
				$redirect_url = $_SERVER['HTTP_REFERER'];
			} else {
				$redirect_url = '/chromed';
			}
			header("Location: $redirect_url");
		}
	}
	public function index()
	{
		$this->portfolio();
	}
	function portfolio($categoryid=''){
		$categoryid = (isset($_GET['categoryid']))? $_GET['categoryid']:$categoryid;
		if(empty($_POST)) {
			// $data['category'] = $this->category->get(array('where'=>array('categoryid'=>$categoryid)));
			// $data['category_children'] = $this->category->get(array('where'=>array('parentid'=>$categoryid)));
			$categories = $this->category->get();
			$cat_rel=array();
			if(!empty($categories)) {
				$categories_unsorted=array();
				foreach ($categories as $value) {
					$cat_rel[$value->parentid][$value->categoryid]=$value;				
					$categories_unsorted[$value->categoryid]=$value;
				}
			}
			if(isset($categoryid) && $categoryid>0) {
				$data['categoryid']=$categoryid;
			} else {
				$categoryid=0;	
				$data['categoryid']=0;
			}		
			$data['categories']=$cat_rel;
			$data['categories_unsorted']=$categories_unsorted;


			$data['sections'] = array($this->load->view('manage_portfolio',$data,true));
			$this->load->view('main',$data);
		} else {
			$this->load->model('user');
			$validate = $this->user->validate_user_credentials($_POST);
			if($validate){
				if($validate[0]->status=='active'){
					//set session
					$this->session_login($validate[0]);
					header('Location: /chromed/manage');
				}
			}
			$data['login_error']='Invalid Email / Password';
			$data['sections'] = array($this->load->view('login',$data,true));
			$this->load->view('main',$data);
		}
	}
}