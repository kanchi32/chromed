<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->library('session');		
	}
public function index()
{
	if(empty($_POST)) {
		$data['sections'] = array($this->load->view('contact','',true));
		$this->load->view('main',$data);
	} else {
		$this->load->model('user');
		$validate = $this->user->validate_user_credentials($_POST);
		if($validate){
			if($validate[0]->status=='active'){
					//set session								
				$this->session_login($validate[0]);
				header('Location: /chromed/manage');
			}
		}
		$data['login_error']='Invalid Email / Password';
		$data['sections'] = array($this->load->view('contact',$data,true));
		$this->load->view('main',$data);
	}
}
}
