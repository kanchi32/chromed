<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class About extends CI_Controller {
	public function index()
	{
		$data['sections'] = array($this->load->view('about','',true));
		$this->load->view('main',$data);
	}
}
