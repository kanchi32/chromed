<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function index()
	{
		$categoryid = 2;
		$data=array();
		$this->load->model('category');
		$categories = $this->category->get();
		$cat_rel=array();
		if(!empty($categories)) {
			$categories_unsorted=array();
			foreach ($categories as $value) {
				$cat_rel[$value->parentid][$value->categoryid]=$value;				
				$categories_unsorted[$value->categoryid]=$value;
			}
		}
		if(isset($categoryid) && $categoryid>"0") {
			$data['categoryid']=$categoryid;
		} else if($categoryid=="0"){
			// $data['categoryid']=$cat_rel[];
		}
		$data['categories']=$cat_rel;
		$data['categories_unsorted']=$categories_unsorted;						
		if(isset($cat_rel[$categoryid])){ //this category is parent of some other category.
			$data['sections'] = array($this->load->view('home',$data,true));
		} else {			
			$data['sections'] = array($this->load->view('home',$data,true));
		}
		$this->load->view('main',$data);		
	}
}
